<?php

namespace App\Controller;

use App\Service\MovieService;
use App\Service\ParametersValidatorService;
use Exception;
use App\Entity\Error\ErrorType;
use App\Exception\ApiException;
use App\Service\LoggerService;
use GuzzleHttp\Exception\GuzzleException;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MovieController
 * @package App\Controller
 */
class MovieController extends AbstractController
{
    /**
     * @Route(name="get_movie_entity", path="/movies/{id}", methods={"GET"})
     * @param int $id
     * @param MovieService $movieService
     * @param LoggerService $loggerService
     * @return JsonResponse
     */
    public function getMovie(
        int $id,
        MovieService $movieService,
        LoggerService $loggerService
    ): JsonResponse {
        $loggerService->log(__CLASS__, __FUNCTION__, 'Begin', Logger::INFO);
        try {
            $movie = $movieService->getMovie($id);
            $loggerService->log(__CLASS__, __FUNCTION__, 'End', Logger::INFO);
            return $this->getJsonResponse(JsonResponse::HTTP_OK, $movie);
        } catch (ApiException $e) {
            $e->setTraceId($loggerService->getLoggerId());
            $loggerService->log(__CLASS__, __FUNCTION__, $e->getLogMessage(), Logger::ERROR);
            return $this->getJsonResponse($e->getHttpCode(), $e);
        } catch (Exception | GuzzleException $e) {
            $loggerService->log(__CLASS__, __FUNCTION__, $e->getMessage(), Logger::ERROR);
            $out = new ApiException(ErrorType::INTERNAL_ERROR, JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->getJsonResponse($out->getHttpCode(), $out);
        }
    }

    /**
     * @Route(name="search_movie_entity", path="/movies", methods={"GET"})
     * @param Request $request
     * @param MovieService $movieService
     * @param LoggerService $loggerService
     * @param ParametersValidatorService $parametersValidatorService
     * @return JsonResponse
     */
    public function searchMovie(
        Request $request,
        MovieService $movieService,
        LoggerService $loggerService,
        ParametersValidatorService $parametersValidatorService
    ): JsonResponse {
        $loggerService->log(__CLASS__, __FUNCTION__, 'Begin', Logger::INFO);
        try {
            $this->setPaginationData($request);
            $data = $request->query->all();
            $parametersValidatorService->checkData(
                $data,
                ['search'],
                [
                    'search' => $parametersValidatorService::STRING,
                ]
            );
            if (key_exists('includeAdult', $data)) {
                $parametersValidatorService->checkSingleData(
                    boolval($data['includeAdult']),
                    'includeAdult',
                    $parametersValidatorService::BOOLEAN
                );
            }
            if (key_exists('year', $data)) {
                $parametersValidatorService->checkSingleData(
                    intval($data['year']),
                    'year',
                    $parametersValidatorService::INTEGER
                );
            }
            $movies = $movieService->searchMovie(
                $data['search'],
                ($data !== null && key_exists('includeAdult', $data)) ? boolval($data['includeAdult']) : false,
                ($data !== null && key_exists('year', $data)) ? intval($data['year']) : false,
            );
            $loggerService->log(__CLASS__, __FUNCTION__, 'End', Logger::INFO);
            return $this->getJsonResponse(JsonResponse::HTTP_OK, $movies);
        } catch (ApiException $e) {
            $e->setTraceId($loggerService->getLoggerId());
            $loggerService->log(__CLASS__, __FUNCTION__, $e->getLogMessage(), Logger::ERROR);
            return $this->getJsonResponse($e->getHttpCode(), $e);
        } catch (Exception | GuzzleException $e) {
            $loggerService->log(__CLASS__, __FUNCTION__, $e->getMessage(), Logger::ERROR);
            $out = new ApiException(ErrorType::INTERNAL_ERROR, JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
            return $this->getJsonResponse($out->getHttpCode(), $out);
        }
    }
}
