<?php

namespace App\Controller;

use App\Exception\Technical\MissingCodeException;
use App\Service\LoggerService;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IsAliveController
 * @package App\Controller
 */
class IsAliveController extends AbstractController
{
    /**
     * @Route(name="is_alive", path="/is-alive", methods={"GET"})
     * @param LoggerService $loggerService
     * @return JsonResponse
     * @throws MissingCodeException
     */
    public function isAlive(
        LoggerService $loggerService
    ): JsonResponse {
        $loggerService->log(__CLASS__, __FUNCTION__, 'Begin', Logger::INFO);
        $loggerService->log(__CLASS__, __FUNCTION__, 'End', Logger::INFO);
        return $this->getJsonResponse(JsonResponse::HTTP_OK);
    }
}
