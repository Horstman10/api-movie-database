<?php

namespace App\Controller;

use App\Entity\Response\Response;
use App\Exception\Parameter\ConstraintNotFoundException;
use App\Exception\Parameter\ConstraintViolationParameterException;
use App\Exception\Technical\MissingCodeException;
use App\Service\ParametersValidatorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AbstractController
 * @package App\Controller
 */
abstract class AbstractController extends Controller
{
    /**
     * @param int $httpStatus
     * @param null $data
     * @return JsonResponse
     */
    protected function getJsonResponse(int $httpStatus, $data = null)
    {
        $out = new Response();
        switch ($httpStatus) {
            case JsonResponse::HTTP_OK:
            case JsonResponse::HTTP_CREATED:
            case JsonResponse::HTTP_NO_CONTENT:
                $out->setData($data);
                $out->setSuccess(true);
                break;
            case JsonResponse::HTTP_NOT_FOUND:
            case JsonResponse::HTTP_UNAUTHORIZED:
            case JsonResponse::HTTP_BAD_REQUEST:
            case JsonResponse::HTTP_FORBIDDEN:
            case JsonResponse::HTTP_INTERNAL_SERVER_ERROR:
            case JsonResponse::HTTP_CONFLICT:
                $out->setError($data);
                break;
        }
        if (isset($_SESSION)) {
            if (key_exists('lastPage', $_SESSION) && $_SESSION['lastPage'] !== null) {
                $out->setLastPage($_SESSION['lastPage']);
            }
            if (key_exists('totalOfItems', $_SESSION) && $_SESSION['totalOfItems'] !== null) {
                $out->setTotalOfItems($_SESSION['totalOfItems']);
            }
            if (key_exists('page', $_SESSION) && $_SESSION['page'] !== null) {
                $out->setPage($_SESSION['page']);
            }
            if (key_exists('numberOfItems', $_SESSION) && $_SESSION['numberOfItems'] !== null) {
                $out->setNumberOfItems($_SESSION['numberOfItems']);
            }
            if (key_exists('order', $_SESSION) && $_SESSION['order'] !== null) {
                $out->setOrder($_SESSION['order']);
            }
            if (key_exists('sort', $_SESSION) && $_SESSION['sort'] !== null) {
                $out->setSort($_SESSION['sort']);
            }
            if ($out->getTotalOfItems() < $out->getNumberOfItems()) {
                $out->setNumberOfItems($out->getTotalOfItems());
            }
        }
        return new JsonResponse(json_encode($out), $httpStatus, [], true);
    }

    /**
     * @param Request $request
     */
    protected function setPaginationData(Request $request): void
    {
        $_SESSION['page'] = intval($request->get('page', 1));
        $_SESSION['numberOfItems'] = intval($request->get(
            'numberOfItems',
            20
        ));
    }

    /**
     * @param ParametersValidatorService $parametersValidatorService
     * @param array $data
     * @throws ConstraintNotFoundException
     * @throws ConstraintViolationParameterException
     * @throws MissingCodeException
     */
    protected function checkFilterDate(ParametersValidatorService $parametersValidatorService, array $data)
    {
        ($data !== null && key_exists('createdAtBegin', $data)) ?
            $parametersValidatorService->checkSingleData(
                $data['createdAtBegin'],
                'createdAtBegin',
                $parametersValidatorService::DATE_TIME
            )
            : null;
        ($data !== null && key_exists('createdAtEnd', $data)) ?
            $parametersValidatorService->checkSingleData(
                $data['createdAtEnd'],
                'createdAtEnd',
                $parametersValidatorService::DATE_TIME
            )
            : null;
        ($data !== null && key_exists('updatedAtBegin', $data)) ?
            $parametersValidatorService->checkSingleData(
                $data['updatedAtBegin'],
                'updatedAtBegin',
                $parametersValidatorService::DATE_TIME
            )
            : null;
        ($data !== null && key_exists('updatedAtEnd', $data)) ?
            $parametersValidatorService->checkSingleData(
                $data['updatedAtEnd'],
                'updatedAtEnd',
                $parametersValidatorService::DATE_TIME
            )
            : null;
        ($data !== null && key_exists('deletedAtBegin', $data)) ?
            $parametersValidatorService->checkSingleData(
                $data['deletedAtBegin'],
                'deletedAtBegin',
                $parametersValidatorService::DATE_TIME
            )
            : null;
        ($data !== null && key_exists('deletedAtEnd', $data)) ?
            $parametersValidatorService->checkSingleData(
                $data['deletedAtEnd'],
                'deletedAtEnd',
                $parametersValidatorService::DATE_TIME
            )
            : null;
    }
}
