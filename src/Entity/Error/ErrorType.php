<?php

namespace App\Entity\Error;

/**
 * Class ErrorType
 * @package App\Entity\Error
 */
class ErrorType
{
    //internal
    public const API_ACCESS_FORBIDDEN = 'api_access_forbidden';
    public const FAKE_ENTITY_NOT_FOUND = 'fake_entity_not_found_%s';
    public const INTERNAL_ERROR = 'internal_error';
    public const MISSING_EXCEPTION_CODE = 'undefined_code_for_exception';

    // Parameters
    public const CONSTRAINT_NOT_FOUND = 'constraint_not_found_%s';
    public const CONSTRAINT_VIOLATION_PARAMETER = 'field_%s_%s';
    public const EMPTY_DATA = 'fields_not_provided';
    public const INVALID_PARAMETER = 'field_%s_not_provided';

    // Movies
    public const MOVIE_NOT_FOUND_ID = 'movie_not_found_%id';
    public const MOVIE_LIST_NOT_FOUND_ID = 'movie_list_not_found_%id';

    public const ERROR_CODES = [
        // Internal (0 - 99)
        self::INTERNAL_ERROR => 1,
        self::MISSING_EXCEPTION_CODE => 2,
        self::API_ACCESS_FORBIDDEN => 3,
        self::FAKE_ENTITY_NOT_FOUND => 4,
        // Parameters (100 - 199)
        self::EMPTY_DATA => 100,
        self::CONSTRAINT_VIOLATION_PARAMETER => 101,
        self::INVALID_PARAMETER => 102,
        self::CONSTRAINT_NOT_FOUND => 103,
        // Movie (200 - 299)
        self::MOVIE_NOT_FOUND_ID => 200,
        self::MOVIE_LIST_NOT_FOUND_ID => 201,
    ];
}
