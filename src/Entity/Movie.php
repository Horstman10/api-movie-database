<?php

namespace App\Entity;

use DateTime;
use Exception;
use JsonSerializable;

/**
 * Class Movie
 * @package App\Entity
 */
class Movie implements JsonSerializable
{
    private const HYDRATE_DATA = [
        'id' => "id",
        'imdbId' => "imdb_id",
        'title' => "title",
        'posterUrl' => "poster_path",
        'releaseYear' => "release_date",
        'ratingValue' => "vote_average",
        'ratingCount' => "vote_count",
    ];

    /**
     * @var int|null
     */
    private ?int $id = null;

    /**
     * @var string|null $imdbId
     */
    private ?string $imdbId = null;

    /**
     * @var string|null $title
     */
    private ?string $title = null;

    /**
     * @var string|null $posterUrl
     */
    private ?string $posterUrl = null;

    /**
     * @var int|null $releaseYear
     */
    private ?int $releaseYear = null;

    /**
     * @var float|null $ratingValue
     */
    private ?float $ratingValue = null;

    /**
     * @var float|null $ratingCount
     */
    private ?float $ratingCount = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Movie
     */
    public function setId(?int $id): Movie
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImdbId(): ?string
    {
        return $this->imdbId;
    }

    /**
     * @param string|null $imdbId
     * @return Movie
     */
    public function setImdbId(?string $imdbId): Movie
    {
        $this->imdbId = $imdbId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return Movie
     */
    public function setTitle(?string $title): Movie
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPosterUrl(): ?string
    {
        return $this->posterUrl;
    }

    /**
     * @param string|null $posterUrl
     * @return Movie
     */
    public function setPosterUrl(?string $posterUrl): Movie
    {
        $this->posterUrl = $posterUrl;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getReleaseYear(): ?int
    {
        return $this->releaseYear;
    }

    /**
     * @param int|null $releaseYear
     * @return Movie
     */
    public function setReleaseYear(?int $releaseYear): Movie
    {
        $this->releaseYear = $releaseYear;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getRatingValue(): ?float
    {
        return $this->ratingValue;
    }

    /**
     * @param float|null $ratingValue
     * @return Movie
     */
    public function setRatingValue(?float $ratingValue): Movie
    {
        $this->ratingValue = $ratingValue;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getRatingCount(): ?float
    {
        return $this->ratingCount;
    }

    /**
     * @param float|null $ratingCount
     * @return Movie
     */
    public function setRatingCount(?float $ratingCount): Movie
    {
        $this->ratingCount = $ratingCount;
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->getId(),
            'imdbId' => $this->getImdbId(),
            'title' => $this->getTitle(),
            'posterUrl' => $this->getPosterUrl(),
            'releaseYear' => $this->getReleaseYear(),
            'ratingValue' => $this->getRatingValue(),
            'ratingCount' => $this->getRatingCount(),
        ];
    }

    /**
     * @param array $movieData
     * @return $this
     * @throws Exception
     */
    public function hydrate(array $movieData): self
    {
        foreach (self::HYDRATE_DATA as $key => $value) {
            if (key_exists($value, $movieData) && !empty($movieData[$value])) {
                switch ($key) {
                    case 'id':
                    case 'ratingCount':
                        $this->$key = intval($movieData[$value]);
                        break;
                    case 'ratingValue':
                        $this->$key = floatval($movieData[$value]);
                        break;
                    case 'releaseYear':
                        $releaseDate = new DateTime($movieData[$value]);
                        $this->$key = $releaseDate->format('Y');
                        break;
                    default:
                        $this->$key = $movieData[$value];
                        break;
                }
            }
        }

        return $this;
    }
}
