<?php

namespace App\Entity\Response;

use App\Exception\ApiException;
use JsonSerializable;

/**
 * Class Response
 * @package App\Entity\Response
 */
class Response implements JsonSerializable
{
    /**
     * @var bool $success
     */
    private $success;

    /**
     * @var mixed|null $data
     */
    private $data;

    /**
     * @var ApiException|null $error
     */
    private $error;

    /**
     * @var int|null $page
     */
    private $page;

    /**
     * @var int|null $lastPage
     */
    private $lastPage;

    /**
     * @var int|null $numberOfItems
     */
    private $numberOfItems;

    /**
     * @var int|null $totalOfItems
     */
    private $totalOfItems;

    /**
     * @var string|null $sort
     */
    private $sort;

    /**
     * @var string|null $order
     */
    private $order;

    /**
     * Response constructor.
     * @param bool $success
     */
    public function __construct(bool $success = false)
    {
        $this->success = $success;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     * @return Response
     */
    public function setSuccess(bool $success): Response
    {
        $this->success = $success;
        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param null $data
     * @return $this
     */
    public function setData($data = null)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return ApiException|null
     */
    public function getError(): ?ApiException
    {
        return $this->error;
    }

    /**
     * @param ApiException|null $error
     * @return Response
     */
    public function setError(?ApiException $error): Response
    {
        $this->error = $error;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPage(): ?int
    {
        return $this->page;
    }

    /**
     * @param int|null $page
     * @return Response
     */
    public function setPage(?int $page): Response
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLastPage(): ?int
    {
        return $this->lastPage;
    }

    /**
     * @param int|null $lastPage
     * @return Response
     */
    public function setLastPage(?int $lastPage): Response
    {
        $this->lastPage = $lastPage;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getNumberOfItems(): ?int
    {
        return $this->numberOfItems;
    }

    /**
     * @param int|null $numberOfItems
     * @return Response
     */
    public function setNumberOfItems(?int $numberOfItems): Response
    {
        $this->numberOfItems = $numberOfItems;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSort(): ?string
    {
        return $this->sort;
    }

    /**
     * @param string|null $sort
     * @return Response
     */
    public function setSort(?string $sort): Response
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrder(): ?string
    {
        return $this->order;
    }

    /**
     * @param string|null $order
     * @return Response
     */
    public function setOrder(?string $order): Response
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTotalOfItems(): ?int
    {
        return $this->totalOfItems;
    }

    /**
     * @param int|null $totalOfItems
     * @return Response
     */
    public function setTotalOfItems(?int $totalOfItems): Response
    {
        $this->totalOfItems = $totalOfItems;
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'success' => $this->isSuccess(),
            'data' => $this->getData(),
            'error' => $this->getError(),
            'page' => $this->getPage(),
            'lastPage' => $this->getLastPage(),
            'numberOfItems' => $this->getNumberOfItems(),
            'totalOfItems' => $this->getTotalOfItems(),
            'sort' => $this->getSort(),
            'order' => $this->getOrder(),
        ];
    }
}
