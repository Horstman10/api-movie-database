<?php

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class ApiService
 * @package App\Service\Api
 */
abstract class ApiService
{
    public const PARAMS_IN_JSON = 'json';
    public const PARAMS_IN_QUERY = 'query';
    /**
     * @var Client $guzzleClient
     */
    protected $guzzleClient;

    /**
     * @var UrlGeneratorInterface
     */
    protected $router;

    /**
     * @var LoggerService $logger
     */
    protected $logger;

    /**
     * @var string
     */
    protected $baseUri;

    /**
     * ApiService constructor.
     * @param Client $guzzleClient
     * @param LoggerService $logger
     * @param UrlGeneratorInterface $router
     * @param string $baseUri
     */
    public function __construct(
        Client $guzzleClient,
        LoggerService $logger,
        UrlGeneratorInterface $router,
        string $baseUri
    ) {
        $this->guzzleClient = $guzzleClient;
        $this->logger = $logger;
        $this->router = $router;
        $this->baseUri = $baseUri;
    }

    /**
     * @param string $route
     * @param string $methodCall
     * @param array $params
     * @param string|null $methodParam
     * @param array|null $headers
     * @return ResponseInterface
     * @throws GuzzleException
     */
    protected function call(
        string $route,
        string $methodCall,
        array $params,
        ?string $methodParam = null,
        array $headers = null
    ): ResponseInterface {
        $url = $this->baseUri . $this->router->generate(
            $route,
            $params
        );
        $params = $this->addPaginationParameters($params, $methodParam);
        $this->logger->log(__CLASS__, __FUNCTION__, 'Begin');
        if ($methodParam != null) {
            $paramSend = [
                $methodParam => $params
            ];
        } else {
            $paramSend = [];
        }
        $paramSend['headers'] = [];
        if ($headers != null) {
            $paramSend['headers'] = $headers;
        }

        $this->logger->log(__CLASS__, __FUNCTION__, 'End');
        return $this->guzzleClient->request($methodCall, $url, $paramSend);
    }

    /**
     * @param array $params
     * @param string|null $methodParam
     * @return array
     */
    public function addPaginationParameters(
        array $params,
        ?string $methodParam = null
    ): array {
        $this->logger->log(__CLASS__, __FUNCTION__, 'Begin');
        if (isset($_SESSION) && $methodParam === self::PARAMS_IN_QUERY && $_SESSION !== null && sizeof($_SESSION) > 0) {
            if (key_exists('numberOfItems', $_SESSION) && $_SESSION['numberOfItems'] !== null) {
                $params['numberOfItems'] = $_SESSION['numberOfItems'];
            }
        }
        $this->logger->log(__CLASS__, __FUNCTION__, 'End');
        return $params;
    }
}
