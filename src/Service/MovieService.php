<?php

namespace App\Service;

use App\Entity\Movie;
use App\Exception\Movie\MovieListNotFound;
use App\Exception\Movie\MovieNotFound;
use App\Exception\Technical\MissingCodeException;
use App\Exception\Technical\TechnicalErrorException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Monolog\Logger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class MovieService
 * @package App\Service
 */
class MovieService extends ApiService
{
    /**
     * @var string $apiKey
     */
    private string $apiKey;

    /**
     * MovieService constructor.
     * @param Client $guzzleClient
     * @param LoggerService $logger
     * @param UrlGeneratorInterface $router
     * @param string $baseUri
     * @param string $apiKey
     */
    public function __construct(
        Client $guzzleClient,
        LoggerService $logger,
        UrlGeneratorInterface $router,
        string $baseUri,
        string $apiKey
    ) {
        parent::__construct(
            $guzzleClient,
            $logger,
            $router,
            $baseUri
        );
        $this->apiKey = $apiKey;
    }

    /**
     * @param int $id
     * @return Movie
     * @throws GuzzleException
     * @throws MovieNotFound
     * @throws TechnicalErrorException
     * @throws MissingCodeException
     */
    public function getMovie(
        int $id
    ): Movie {
        $this->logger->log(__CLASS__, __FUNCTION__, 'Begin');
        $routeName = 'api.movie.get_id';
        $getParams = array(
            'id' => $id,
            'api_key' => $this->apiKey
        );

        $movieResult = $this->call(
            $routeName,
            Request::METHOD_GET,
            $getParams,
            self::PARAMS_IN_QUERY,
            $getParams
        );

        if (Response::HTTP_NOT_FOUND === $movieResult->getStatusCode()) {
            $this->logger->log(__CLASS__, __FUNCTION__, $movieResult->getBody(), Logger::ERROR);
            throw new MovieNotFound($id);
        } elseif (
            Response::HTTP_CREATED !== $movieResult->getStatusCode()
            && Response::HTTP_OK !== $movieResult->getStatusCode()
        ) {
            $this->logger->log(__CLASS__, __FUNCTION__, $movieResult->getBody(), Logger::ERROR);
            throw new TechnicalErrorException();
        }
        $movie = new Movie();
        $movie->hydrate(json_decode($movieResult->getBody(), true));
        $this->logger->log(__CLASS__, __FUNCTION__, 'End');
        return $movie;
    }

    /**
     * @param string $dataSearch
     * @param bool $includeAdult
     * @param int|null $year
     * @return array
     * @throws GuzzleException
     * @throws MovieListNotFound
     * @throws TechnicalErrorException
     */
    public function searchMovie(
        string $dataSearch,
        bool $includeAdult = false,
        ?int $year = null
    ): array {
        $this->logger->log(__CLASS__, __FUNCTION__, 'Begin');
        $routeName = 'api.movie.search';
        $getParams = array(
            'api_key' => $this->apiKey,
            'query' => $dataSearch,
            'include_adult' => $includeAdult,
            'page' => $_SESSION['page']
        );
        if ($year !== null) {
            $getParams['year'] = $year;
        }
        $moviesRequest = $this->call(
            $routeName,
            Request::METHOD_GET,
            $getParams,
            self::PARAMS_IN_QUERY,
            $getParams
        );

        if (Response::HTTP_NOT_FOUND === $moviesRequest->getStatusCode()) {
            $this->logger->log(__CLASS__, __FUNCTION__, $moviesRequest->getBody(), Logger::ERROR);
            throw new MovieListNotFound();
        } elseif (
            Response::HTTP_CREATED !== $moviesRequest->getStatusCode()
            && Response::HTTP_OK !== $moviesRequest->getStatusCode()
        ) {
            $this->logger->log(__CLASS__, __FUNCTION__, $moviesRequest->getBody(), Logger::ERROR);
            throw new TechnicalErrorException();
        }
        $moviesResult = json_decode($moviesRequest->getBody(), true);
        $movies = [];
        foreach ($moviesResult['results'] as $movieResult) {
            $movie = new Movie();
            $movie->hydrate($movieResult);
            $movies[] = $movie;
        }
        $_SESSION['page'] = $moviesResult['page'];
        $_SESSION['totalOfItems'] = $moviesResult['total_results'];
        $_SESSION['lastPage'] = $moviesResult['total_pages'];
        $this->logger->log(__CLASS__, __FUNCTION__, 'End');
        return $movies;
    }
}
