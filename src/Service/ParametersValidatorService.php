<?php

namespace App\Service;

use App\Exception\Parameter\ConstraintNotFoundException;
use App\Exception\Parameter\ConstraintViolationParameterException;
use App\Exception\Parameter\EmptyDataException;
use App\Exception\Parameter\InvalidParameterException;
use App\Exception\Technical\MissingCodeException;
use Monolog\Logger;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

/**
 * Class ParametersValidatorService
 * @package App\Service
 */
class ParametersValidatorService
{
    public const CONSTRAINT = [
        'string' => self::STRING,
        'dateTime' => self::DATE_TIME,
        'ip' => self::IP,
        'int' => self::INTEGER,
        'int_blank' => self::INTEGER_BLANK,
        'bool' => self::BOOLEAN,
        'file' => self::FILE,
        'float' => self::FLOAT,
        'array' => self::ARRAY,
        'array_blank' => self::ARRAY_BLANK,
        'email' => self::EMAIL,
        'object' => self::OBJECT,
        'string_blank' => self::STRING_BLANK,
    ];

    public const STRING = 'string';
    public const DATE_TIME = 'dateTime';
    public const IP = 'ip';
    public const INTEGER = 'int';
    public const INTEGER_BLANK = 'int_blank';
    public const BOOLEAN = 'bool';
    public const FILE = 'file';
    public const FLOAT = 'float';
    public const ARRAY = 'array';
    public const ARRAY_BLANK = 'array_blank';
    public const EMAIL = 'email';
    public const OBJECT = 'object';
    public const STRING_BLANK = 'string_blank';

    /**
     * @var LoggerService $logger
     */
    protected $logger;

    /**
     * ParametersValidatorService constructor.
     * @param LoggerService $logger
     */
    public function __construct(
        LoggerService $logger
    ) {
        $this->logger = $logger;
    }

    /**
     * @param $data
     * @param string $dataName
     * @param string|null $constraintsName
     * @param array|null $constraintsOptions
     * @throws ConstraintNotFoundException
     * @throws ConstraintViolationParameterException
     * @throws MissingCodeException
     */
    public function checkSingleData(
        $data,
        string $dataName,
        string $constraintsName = null,
        array $constraintsOptions = null
    ) {
        $this->logger->log(__CLASS__, __FUNCTION__, 'Begin');
        $constraints = $this->getConstraints($constraintsName, $constraintsOptions);
        $validator = Validation::createValidator();
        $violations = $validator->validate($data, $constraints);
        if (0 !== count($violations)) {
            // there are errors, now you can show them
            foreach ($violations as $violation) {
                $e = new ConstraintViolationParameterException($dataName, $violation->getMessage());
                $this->logger->log(__CLASS__, __FUNCTION__, $e->getLogMessage(), Logger::ERROR);
                throw $e;
            }
        }
        $this->logger->log(__CLASS__, __FUNCTION__, 'End');
    }

    /**
     * @param array|null $data
     * @param array $fields
     * @param array $constraintsType
     * @param array $constraintsOptions
     * @return array
     * @throws ConstraintNotFoundException
     * @throws ConstraintViolationParameterException
     * @throws EmptyDataException
     * @throws InvalidParameterException
     * @throws MissingCodeException
     */
    public function checkData(
        ?array $data,
        array $fields,
        array $constraintsType = [],
        array $constraintsOptions = []
    ): array {
        $this->logger->log(__CLASS__, __FUNCTION__, 'Begin');
        $checkedData = array();
        if (empty($data)) {
            $e = new EmptyDataException();
            $this->logger->log(__CLASS__, __FUNCTION__, $e->getLogMessage(), Logger::ERROR);
            throw $e;
        }
        foreach ($fields as $field) {
            if (!isset($data[$field])) {
                $e = new InvalidParameterException($field);
                $this->logger->log(__CLASS__, __FUNCTION__, $e->getLogMessage(), Logger::ERROR);
                throw $e;
            }
            if (isset($constraintsType[$field]) && !is_null($constraintsType[$field])) {
                $constraints = $this->getConstraints(
                    $constraintsType[$field],
                    (key_exists($field, $constraintsOptions)) ? $constraintsOptions[$field] : null
                );
                $validator = Validation::createValidator();
                $violations = $validator->validate($data[$field], $constraints);
                if (0 !== count($violations)) {
                    // there are errors, now you can show them
                    foreach ($violations as $violation) {
                        $e = new ConstraintViolationParameterException($field, $violation->getMessage());
                        $this->logger->log(__CLASS__, __FUNCTION__, $e->getLogMessage(), Logger::ERROR);
                        throw $e;
                    }
                }
            }
            $checkedData[$field] = $data[$field];
        }
        $this->logger->log(__CLASS__, __FUNCTION__, 'End');
        return $checkedData;
    }

    /**
     * @param string|null $name
     * @param array|null $options
     * @return array
     * @throws ConstraintNotFoundException
     * @throws MissingCodeException
     */
    public function getConstraints(?string $name = null, ?array $options = null): array
    {
        if ($name !== null and !key_exists($name, self::CONSTRAINT)) {
            $e = new ConstraintNotFoundException($name);
            $this->logger->log(__CLASS__, __FUNCTION__, $e->getLogMessage(), Logger::ERROR);
            throw $e;
        }
        $constraints = [];
        switch ($name) {
            case self::STRING:
                $constraints = [
                    new Assert\NotBlank(),
                    new Assert\Type(
                        [
                            'type' => 'string',
                        ]
                    ),
                ];
                break;
            case self::DATE_TIME:
                $constraints = [
                    new Assert\NotBlank(),
                    new Assert\DateTime(),
                ];
                break;
            case self::IP:
                $constraints = [
                    new Assert\NotBlank(),
                    new Assert\Ip(),
                ];
                break;
            case self::BOOLEAN:
                $constraints = [
                    new Assert\Type(
                        [
                            'type' => 'boolean'
                        ]
                    ),
                ];
                break;
            case self::INTEGER:
                $constraints = [
                    new Assert\NotBlank(),
                    new Assert\Type(
                        [
                            'type' => 'integer'
                        ]
                    ),
                ];
                break;
            case self::INTEGER_BLANK:
                $constraints = [
                    new Assert\Type(
                        [
                            'type' => 'integer'
                        ]
                    ),
                ];
                break;
            case self::FILE:
                $constraints = [
                    new Assert\NotBlank(),
                    new Assert\File()
                ];
                break;
            case self::FLOAT:
                $constraints = [
                    new Assert\NotBlank(),
                    new Assert\Type(
                        [
                            'type' => 'float'
                        ]
                    ),
                ];
                break;
            case self::ARRAY:
                $constraints = [
                    new Assert\NotBlank(),
                    new Assert\Type(
                        [
                            'type' => 'array'
                        ]
                    ),
                ];
                break;
            case self::ARRAY_BLANK:
                $constraints = [
                    new Assert\Type(
                        [
                            'type' => 'array'
                        ]
                    ),
                ];
                break;
            case self::EMAIL:
                $constraints = [
                    new Assert\Email(),
                ];
                break;
            case self::OBJECT:
                $constraints = [
                    new Assert\Type(
                        [
                            'type' => 'object'
                        ]
                    ),
                ];
                break;
            case self::STRING_BLANK:
                $constraints = [
                    new Assert\Type(
                        [
                            'type' => 'string'
                        ]
                    ),
                ];
                break;
        }
        if ($options !== null) {
            foreach ($options as $option) {
                $constraints[] = $option;
            }
        }
        return $constraints;
    }
}
