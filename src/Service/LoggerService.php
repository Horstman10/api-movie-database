<?php

namespace App\Service;

use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class LoggerService
{
    public const INTERNAL_LOGGER_ID = 'HTTP_X_HORSTMAN_INTERNAL_LOGGER_ID';
    /**
     * @var LoggerInterface $logger
     */
    private $logger;

    /**
     * @var string|null $loggerId
     */
    public $loggerId;


    /**
     * LoggerService constructor.
     * @param RequestStack $request
     * @param LoggerInterface $logger
     */
    public function __construct(RequestStack $request, LoggerInterface $logger)
    {
        if ($request !== null && $request->getCurrentRequest() !== null) {
            $this->loggerId = $request->getCurrentRequest()->server->get(
                self::INTERNAL_LOGGER_ID,
                uniqid('', true)
            );
        } else {
            //for message
            $this->loggerId = uniqid('', true);
        }
        $this->logger = $logger;
    }

    /**
     * @param string $className
     * @param string $functionName
     * @param string|null $message
     * @param int $level
     */
    public function log(
        string $className,
        string $functionName,
        ?string $message,
        int $level = Logger::INFO
    ): void {
        $message = ($message === null) ? "Internal error" : $message;
        $this->logger->log(
            $level,
            $this->loggerId . ' : ' . $className . ' : ' . $functionName . ' : ' . $message
        );
    }

    /**
     * @return string|null
     */
    public function getLoggerId(): ?string
    {
        return $this->loggerId;
    }

    /**
     * @param string|null $loggerId
     * @return LoggerService
     */
    public function setLoggerId(?string $loggerId): LoggerService
    {
        $this->loggerId = $loggerId;
        return $this;
    }
}
