<?php

namespace App\Exception\Technical;

use Exception;

/**
 * Class TechnicalErrorException
 * @package App\Exception
 */
class TechnicalErrorException extends Exception
{

}
