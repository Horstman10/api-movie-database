<?php

namespace App\Exception\Technical;

use App\Entity\Error\ErrorType;
use App\Exception\ApiException;
use Symfony\Component\HttpFoundation\JsonResponse;

class MissingCodeException extends ApiException
{
    protected $message = ErrorType::MISSING_EXCEPTION_CODE;
    protected $httpCode = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;

    /**
     * MissingCodeException constructor.
     * @param string $previousErrorMessage
     * @throws MissingCodeException
     */
    public function __construct($previousErrorMessage = '')
    {
        parent::__construct(
            $this->message,
            $this->httpCode,
            []
        );
        $this->params[] = $previousErrorMessage;
    }
}
