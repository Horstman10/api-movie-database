<?php

namespace App\Exception\Parameter;

use App\Entity\Error\ErrorType;
use App\Exception\ApiException;
use App\Exception\Technical\MissingCodeException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class ConstraintNotFoundException
 * @package App\Exception\Parameter
 */
class ConstraintNotFoundException extends ApiException
{
    protected $message = ErrorType::CONSTRAINT_NOT_FOUND;
    protected $httpCode = JsonResponse::HTTP_NOT_FOUND;

    /**
     * ConstraintNotFoundException constructor.
     * @param string $name
     * @throws MissingCodeException
     */
    public function __construct(string $name = '')
    {
        parent::__construct($this->message, $this->httpCode, [$name]);
    }
}
