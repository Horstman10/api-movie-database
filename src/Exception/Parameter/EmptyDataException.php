<?php

namespace App\Exception\Parameter;

use App\Entity\Error\ErrorType;
use App\Exception\ApiException;
use App\Exception\Technical\MissingCodeException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class EmptyDataException
 * @package App\Exception\Parameter
 */
class EmptyDataException extends ApiException
{
    protected $message = ErrorType::EMPTY_DATA;
    protected $httpCode = JsonResponse::HTTP_BAD_REQUEST;

    /**
     * EmptyDataException constructor.
     * @throws MissingCodeException
     */
    public function __construct()
    {
        parent::__construct($this->message, $this->httpCode);
    }
}
