<?php

namespace App\Exception\Parameter;

use App\Entity\Error\ErrorType;
use App\Exception\ApiException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class InvalidParameterException
 * @package App\Exception\Parameter
 */
class InvalidParameterException extends ApiException
{
    protected $message = ErrorType::INVALID_PARAMETER;
    protected $httpCode = JsonResponse::HTTP_BAD_REQUEST;

    public function __construct(string $fieldName = '')
    {
        parent::__construct($this->message, $this->httpCode, [$fieldName]);
    }
}
