<?php

namespace App\Exception\Movie;

use App\Entity\Error\ErrorType;
use App\Exception\ApiException;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class MovieNotFound
 * @package App\Exception\Movie
 */
class MovieNotFound extends ApiException
{
    protected $message = ErrorType::MOVIE_NOT_FOUND_ID;
    protected $httpCode = JsonResponse::HTTP_NOT_FOUND;

    public function __construct(int $id = 0)
    {
        parent::__construct($this->message, $this->httpCode, [$id]);
    }
}
