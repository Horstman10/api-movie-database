<?php

namespace App\Exception\Movie;

use App\Exception\ApiException;

/**
 * Class MovieException
 * @package App\Exception\Movie
 */
class MovieException extends ApiException
{
}
