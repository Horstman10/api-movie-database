<?php

namespace App\Exception;

use Exception;
use App\Entity\Error\ErrorType;
use App\Exception\Technical\MissingCodeException;
use JsonSerializable;

/**
 * Class ApiException
 * @package App\Exception
 */
class ApiException extends Exception implements JsonSerializable
{
    /**
     * @var int $httpCode
     */
    protected $httpCode;

    /**
     * @var array $params
     */
    protected $params;

    /**
     * @var string|null $traceId
     */
    protected $traceId;

    /**
     * @return int
     */
    public function getHttpCode(): int
    {
        return $this->httpCode;
    }

    /**
     * @param int $httpCode
     */
    public function setHttpCode(int $httpCode): void
    {
        $this->httpCode = $httpCode;
    }

    /**
     * ApiException constructor.
     * @param string $message
     * @param int $httpCode
     * @param array $params
     * @throws MissingCodeException
     */
    public function __construct(string $message = "", int $httpCode = 0, array $params = [])
    {
        parent::__construct($message, 0, null);
        $this->httpCode = $httpCode;
        $this->params = $params;
        if (!isset(ErrorType::ERROR_CODES[$this->getMessage()])) {
            throw new MissingCodeException($message);
        }
        $this->code = ErrorType::ERROR_CODES[$this->getMessage()];
    }

    /**
     * @return string|null
     */
    public function getTraceId(): ?string
    {
        return $this->traceId;
    }

    /**
     * @param string|null $traceId
     * @return ApiException
     */
    public function setTraceId(?string $traceId): ApiException
    {
        $this->traceId = $traceId;
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'message' => $this->getMessage(),
            'params' => $this->params,
            'code' => $this->code,
            'traceId' => $this->getTraceId(),
        ];
    }

    /**
     * @return string
     */
    public function getLogMessage()
    {
        $out = $this->message;
        $out = vsprintf($out, $this->params);
        return $out;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }
}
