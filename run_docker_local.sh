source .env;

echo "####### docker-compose --verbose  -f docker-compose.yml -f docker-compose.local.yml build --no-cache;";
#docker-compose -f docker-compose.yml -f docker-compose.local.yml --verbose build  --no-cache;
docker-compose -f docker-compose.yml -f docker-compose.local.yml --verbose build;

echo "####### docker-compose -f docker-compose.yml -f docker-compose.local.yml up -d --no-build;";
docker-compose -f docker-compose.yml -f docker-compose.local.yml up -d --no-build;

echo "####### docker exec -it -u ${USER_NAME} ${SERVER_NAME} sh -c \"eval \`ssh-agent -s\` && ssh-add /home/${USER_NAME}/.ssh/id_rsa\";";
docker exec -it -u ${USER_NAME} ${SERVER_NAME} sh -c "eval \`ssh-agent -s\` && ssh-add /home/${USER_NAME}/.ssh/id_rsa";
echo "####### docker exec -it -u ${USER_NAME} ${SERVER_NAME} sh -c \"git config --global user.name \"${LOCAL_GITLAB_USER_NAME}\"\"";
docker exec -it -u ${USER_NAME} ${SERVER_NAME} sh -c "git config --global user.name \"${LOCAL_GITLAB_USER_NAME}\"";
echo "####### docker exec -it -u ${USER_NAME} ${SERVER_NAME} sh -c \"git config --global user.email \"${LOCAL_GITLAB_EMAIL}\"\"";
docker exec -it -u ${USER_NAME} ${SERVER_NAME} sh -c "git config --global user.email \"${LOCAL_GITLAB_EMAIL}\"";