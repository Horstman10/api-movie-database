API api-movie-database
============

Requirements
------------

You need :
- docker
- docker-compose

Local Installation
------------

    git clone https://gitlab.com/Horstman10/api-movie-database.git
    cd api-movie-database
    cp .env.dist .env
    cp phpunit.xml.dist phpunit.xml
    
Database configuration 
------------
    N/A
    
Configuration of .env    
------------
Change .env variable :

 - **Docker**:
    - PROJECTS_PATH: the path of your project (default ./)
    - PROJECT_NAME_DIRECTORY: name of project directory
    - SERVER_NAME: the name of server (default local-api-movie-database)
    - TAG_DOCKER_IMAGE: Docker tag. If you change docker configuration, you must upgrade the tags
 
 - **Local**:
    - LOCAL_GITLAB_EMAIL: your gitlab email
    - LOCAL_GITLAB_USER_NAME: your gitlab user name
    - USER_NAME: your user name (system account)
    - USER_ID: your user id (sustem account)
    
Docker utils Local :
------------    
Create and start docker image:

    ./run_docker_local.sh

See active docker image and container id:

    docker ps
   
Connect to you docker image:

    docker exec -it -u <userName> <containerId> /bin/bash
    
To execute command on your docker image
    
    docker exec -it -u  <userName> <containerId> <command>
    

Branching model
---------------

We have two protected branches:
- **master**: Should only receive hotfix and merge from develop
- **develop**: Should only receive stable developments that can be sent anytime to production.

Any developer can create a branch from develop to make some developments.
No obligation about the branch naming, but you can use the following convention:
**feature/my-feature**.

Deployment
----------

Deploying to *qualif* environment is manually available through
[Gitlab Pipelines](https://gitlab.com/Horstman10/api-movie-database/-/pipelines).

Step for deploy :
 - **test**
    - validate composer.json:
    
            composer validate
        
    - install vendor:
            
            composer install --no-interaction
            
    - check PSR12 validation:
    
            vendor/bin/phpcs src --standard=PSR12  --ignore='Migrations','Kernel.php'
            
    - execute test with coverage *(in progress)*:
    
            php bin/phpunit -c phpunit.xml.dist --coverage-clover=coverage.xml
            
    - verify coverage *(in progress)*:
            
            bin/console test:coverage:check coverage.xml
            
Docker script qualif
-------
- Define source of parameters:

        source .env
        
- Create docker image:

        docker-compose --verbose build ${SERVER_NAME};
        
- tag image of latest version and docker tag version:

        docker image tag ${IMAGE_NAME}  ${IMAGE_NAME}:latest;
        docker image tag ${IMAGE_NAME}  ${IMAGE_NAME}:${TAG_DOCKER_IMAGE};
        
- push image on your private repository 

        docker push ${IMAGE_NAME}:latest;
        docker push ${IMAGE_NAME}:${TAG_DOCKER_IMAGE};
        
- execute ssh to download last image version on qualif and start this image

        ssh root@51.75.25.48 "docker pull ${IMAGE_NAME}:latest && docker stop ${SERVER_NAME} || true && docker run -e PROJECT_NAME_DIRECTORY=api-skeleton -e SERVER_NAME=51.75.25.48  -d -p 10140:80 --rm --name=${SERVER_NAME} ${IMAGE_NAME}:latest  && exit";
            
Testing
-------

The project contains some stable unit tests. They are integrated as recommended by the Symfony project.

To run the tests, do the following:

    bin/phpunit -c phpunit.xml

To run the tests and check the code coverage, you can execute:

    bin/phpunit -c phpunit.xml.dist --coverage-clover=coverage.xml
    bin/console test:coverage:check coverage.xml
<!--
Pre-commit hook
---------------

A somewhat common good practice is to run unit tests as a
[pre-commit hook](https://git-scm.com/book/gr/v2/Customizing-Git-Git-Hooks).
To do so, run the following:

    ln -sr pre-commit.dist .git/hooks/pre-commit
    -->