<?php

namespace App\Tests\Data;

use App\Entity\Movie;
use App\Entity\Response\Response;

/**
 * Class Data
 * @package App\Tests\Data
 */
class Data
{
    /*###############################################
    #                                               #
    #                 RESPONSE                      #
    #                                               #
    ###############################################*/
    /**
     * @param null $data
     * @return Response
     */
    public function getResponseSuccess($data = null) : Response
    {
        $response = new Response();
        $response->setData($data);
        $response->setSuccess(true);
        return $response;
    }

    /**
     * @param null $data
     * @return Response
     */
    public function getResponseError($data = null) : Response
    {
        $response = new Response();
        $response->setError($data);
        $response->setSuccess(false);
        return $response;
    }

    /**
     * @param null $data
     * @return Response
     */
    public function getPaginationResponseSuccess($data = null) : Response
    {
        $response = new Response();
        $response->setData($data)
            ->setSuccess(true)
            ->setPage(1)
            ->setSort('id')
            ->setOrder('asc')
            ->setNumberOfItems(20);
        return $response;
    }

    /**
     * @param null $data
     * @return Response
     */
    public function getPaginationResponseError($data = null) : Response
    {
        $response = new Response();
        $response->setError($data)
            ->setSuccess(false)
            ->setPage(1)
            ->setSort('id')
            ->setOrder('asc')
            ->setNumberOfItems(20);
        return $response;
    }
    /*###############################################
    #                                               #
    #                 ENTITY                        #
    #                                               #
    ###############################################*/

    /**
     * @return Movie
     */
    public function getMovie() : Movie
    {
        $movie = new Movie();
        $movie->setId(1)
            ->setImdbId('imdbId')
            ->setPosterUrl('posterUrl')
            ->setRatingCount(1)
            ->setRatingValue(1.0)
            ->setTitle('title')
            ->setReleaseYear(2000);
        return $movie;
    }

    public function getResponseInterface()
    {
        $reponse = new \GuzzleHttp\Psr7\Response();
    }
}