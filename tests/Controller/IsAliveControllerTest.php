<?php

namespace App\Tests\Controller;

use App\Service\LoggerService;
use App\Tests\Data\Data;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class IsAliveControllerTest
 * @package App\Tests\Controller\Vidal
 */
class IsAliveControllerTest extends WebTestCase
{
    /**
     * @var KernelBrowser $client
     */
    private $client;
    private $loggerServiceMock;
    /**
     * @var Data $data
     */
    private $data;
    public function setUp()
    {
        $_SESSION = [];
        $this->client = static::createClient();
        $this->loggerServiceMock = $this->getMockBuilder(LoggerService::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testIsAlive()
    {
        $this->data = new Data();
        $this->client->getContainer()->set(
            LoggerService::class,
            $this->loggerServiceMock
        );

        $expected = json_decode(json_encode($this->data->getResponseSuccess(null), true), true);
        $this->client->request(
            'GET',
            '/api/v1/is-alive'
        );

        $this->assertEquals(JsonResponse::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(
            $expected,
            json_decode($this->client->getResponse()->getContent(), true)
        );
    }

}
