<?php

namespace App\Tests\Controller;

use App\Entity\Error\ErrorType;
use App\Exception\ApiException;
use App\Exception\Movie\MovieNotFound;
use App\Exception\Technical\MissingCodeException;
use App\Service\LoggerService;
use App\Service\MovieService;
use App\Tests\Data\Data;
use Exception;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;

class MovieControllerTest extends WebTestCase
{
    /**
     * @var KernelBrowser $client
     */
    private $client;
    private $loggerServiceMock;
    private $movieServiceMock;
    /**
     * @var Data $data
     */
    private $data;

    public function setUp(): void
    {
        $_SESSION = [];
        $this->data = new Data();
        $this->client = static::createClient();
        $this->loggerServiceMock = $this->getMockBuilder(LoggerService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->movieServiceMock = $this->getMockBuilder(MovieService::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->client->getContainer()->set(
            MovieService::class,
            $this->movieServiceMock
        );
        $this->client->getContainer()->set(
            LoggerService::class,
            $this->loggerServiceMock
        );
    }

    public function testGetMovieOnSuccess()
    {
        $this->movieServiceMock->expects($this->any())
            ->method('getMovie')
            ->will($this->returnValue($this->data->getMovie()));
        $expected = json_decode(json_encode(
            $this->data->getResponseSuccess($this->data->getMovie()),
            true
        ), true);
        $this->client->request(
            'GET',
            '/api/v1/movies/1'
        );

        $this->assertEquals(JsonResponse::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(
            $expected,
            json_decode($this->client->getResponse()->getContent(), true)
        );
    }

    /**
     * @throws MissingCodeException
     */
    public function testGetMovieOnApiException()
    {
        $e = new MovieNotFound(2);
        $this->movieServiceMock->expects($this->any())
            ->method('getMovie')
            ->will($this->throwException($e));
        $expected = json_decode(json_encode(
            $this->data->getResponseError($e),
            true
        ), true);
        $this->client->request(
            'GET',
            '/api/v1/movies/1'
        );

        $this->assertEquals(JsonResponse::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(
            $expected,
            json_decode($this->client->getResponse()->getContent(), true)
        );
    }

    public function testGetMovieOnException()
    {
        $e = new Exception();
        $this->movieServiceMock->expects($this->any())
            ->method('getMovie')
            ->will($this->throwException($e));
        $expected = json_decode(json_encode(
            $this->data->getResponseError(new ApiException(
                ErrorType::INTERNAL_ERROR,
                JsonResponse::HTTP_INTERNAL_SERVER_ERROR
            )),
            true
        ), true);
        $this->client->request(
            'GET',
            '/api/v1/movies/1'
        );

        $this->assertEquals(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, $this->client->getResponse()->getStatusCode());
        $this->assertEquals(
            $expected,
            json_decode($this->client->getResponse()->getContent(), true)
        );
    }
}
